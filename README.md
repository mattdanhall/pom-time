Pom Time
========

Minimalist pomodoro timer for the command line.

Requires notify-send from `libnotify-bin`.

[icons-in-terminal](https://github.com/sebastiencs/icons-in-terminal) required to show Pomicons (run with `-i` to show icons).

Clone the repository and install with: (You can edit the install location in install.sh if desired)
```
git clone https://gitlab.com/mattdanhall/pom-time.git
cd pom-time
bash install.sh
```

Get pomming with 
```
pomtime
```

![screenshot](pomtime.png)

Written for and tested on Ubuntu only, let me know if it works elsewhere.