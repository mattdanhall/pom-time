#!/bin/bash
#
#  ----------------------------------------------------------------------------
#  "THE BEER-WARE LICENSE" (Revision 43):
#  <mattdanhall@duck.com> wrote this file.  As long as you retain this notice
#  you can do whatever you want with this stuff. If we meet some day, and you
#  think this stuff is worth it, you can buy me a beer in return.        Matt
#  ----------------------------------------------------------------------------
# 

INSTALL=$HOME/.local

cp -r ./pomtime $INSTALL/
chmod +x $INSTALL/pomtime/pomtime.sh
ln -s $INSTALL/pomtime/pomtime.sh $INSTALL/bin/pomtime

echo -e "Pomtime installed!\nType 'pomtime' to get pomming."