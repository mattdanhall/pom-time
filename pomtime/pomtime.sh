#!/bin/bash
#
#  ----------------------------------------------------------------------------
#  "THE BEER-WARE LICENSE" (Revision 43):
#  <mattdanhall@duck.com> wrote this file.  As long as you retain this notice
#  you can do whatever you want with this stuff. If we meet some day, and you
#  think this stuff is worth it, you can buy me a beer in return.        Matt
#  ----------------------------------------------------------------------------
# 

HERE=$HOME/.local/pomtime/

notify () {
    notify-send "$1" "$2" -u normal -a 'Pom Time'
    paplay $HERE/finish.oga
}

timer () {
  time=$1
  while [ $time -ge 0 ]; do
    printf "\r %*s \r$2%02d:%02d" $(($COLUMNS-8)) " " $(($time/60)) $(($time%60))
    time=$((time-1))
    sleep 1
  done
  printf "\n"
}

exitcommand () {
    clear
    printf "$1Good pomming!\n"
    exit 1
}

input=${1:-'x'}
if [ $input != '-i' ]; then
    icons=' \b'
fi
clear

trap 'exitcommand "${icons:-\ueace  }"' SIGINT
echo -e "Starting Pom Time ${icons:-\uead0} "
paplay $HERE/start.oga
counter=0
while true; do
    counter=$((counter+1))
    read -e -t1 -s
    printf "${icons:-\ueacf  }"
    read -p "Press key to begin pom $counter... " -n1 -s
    paplay $HERE/start.oga
    now=`date +"%H:%M"`
    echo -e "\r${icons:-\ueacf  }Starting pom number $counter at $now"
    timer 1500 ${icons:-'\uead1  '}
    if [ $((counter%4)) -ne 0 ]; then
        echo -e "${icons:-\ueace  }Pom number $counter done, time for a short break"
        notify 'Short break time' 'Step away from the screen'
        timer 300 ${icons:-'\uead2  '}
    else
        echo -e "${icons:-\ueace  }Four poms done, time for a long break"
        notify 'Long break time' 'Good job! Have a rest'
        timer 900 ${icons:-'\uead3  '}
    fi
    notify "Time to work" "Focus"
done

clear
exit 0
